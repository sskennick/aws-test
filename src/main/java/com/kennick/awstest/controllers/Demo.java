package com.kennick.awstest.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/demo")
public class Demo {

    @GetMapping(value = "/demos")
    public String getDemoData() {
        return "Data! ver 2.0";
    }

}
