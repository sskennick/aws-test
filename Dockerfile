# Start with a base image - in this case JDK 8 Alpine
FROM openjdk:8-jdk-alpine as build
WORKDIR ./build/libs/
COPY . app.jar
# Set ENTRYPOINT in exec form to run the container as an executable
ENTRYPOINT ["java","-jar","/app.jar"]